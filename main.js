require('./styles/shows.sass');
require('./styles/bootstrap.css');
require('url-search-params-polyfill');
const Vue = require('./vue.min.js');

// loading shows.json data
function loadJSON(callback) {

    var jsonObj = new XMLHttpRequest();
    jsonObj.overrideMimeType("application/json");
    jsonObj.open('GET', 'shows.json', false); //file read synchronous
    jsonObj.onreadystatechange = function() {
        if (jsonObj.readyState == 4 && jsonObj.status == "200") {
            // .open will NOT return a value but simply returns undefined in async mode so use a callback
            callback(jsonObj.responseText);
        }
    }
    jsonObj.send(null);
}

var jsonresponse = {};
var jsonlist = [];

// Call to read JSON file function with anonymous callback
loadJSON(function(response) {
    jsonresponse = JSON.parse(response);
    for (var item in jsonresponse) {
      jsonlist.push(jsonresponse[item])
    }
});


// initial featured media item upon page load, pending existing query param
var urlParams = new URLSearchParams(window.location.search);
if (urlParams.has('id')) {
  var featuredVideoItem = jsonlist[urlParams.get('id')];
} else {
  var featuredVideoItem = jsonlist[0]
}


function updateURL(videoID) {
  var currentUrl = window.location.href;
  // saving state for back button
  history.pushState(null, null, currentUrl);
  var queryParamInsert = currentUrl.split('?');
  var newPage = queryParamInsert[0] + '?id=' + videoID;
  window.location.replace(newPage);
  var newPage = '';
}

function featuredUpdater(videoID) {
  updateURL(videoID);
  featuredVideoItem = jsonlist[videoID];
  vm.featured = featuredVideoItem;
}

// instantiating Vue
const vm = new Vue({
  el: '#app',
  data: {
    featured: featuredVideoItem,
    medialist: jsonlist,
    showFeaturedVideo: true
  },
  methods: {
    submit: function(e) {
      e.stopPropagation();
      console.log(e.target);
      console.log(e.target.id)
      featuredUpdater(e.target.id)
    }
  }
})
